import React from 'react';
import Todo from './components/Todo';

function App() {
  return (
    <div className="box">
      <Todo />
    </div>
  );
}

export default App;

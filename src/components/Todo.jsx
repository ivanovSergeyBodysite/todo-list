import React, { useState } from 'react';
import AddTodoItem from './AddTodoItem';
import TodoList from './TodoList';


function Todo() {
  const [list, setList] = useState([
    {id: 1, title: 'Store banana', completed: false},
    {id: 2, title: 'Game', completed: true},
  ]);

  function completeItem(id) {
    setList(prevState => {
      return prevState
        .map(item => {
          if (item.id === id) {
            return {...item, completed: !item.completed};
          }
          return item;
        });
    });
  }

  function changeItem(id, newTitle) {
    setList(prevState => {
      return prevState
        .map(item => {
          if (item.id === id) {
            return {...item, title: newTitle};
          }
          return item;
        });
    });
  }

  function addItem(title) {
    if (!title) return;

    setList(prevState => prevState.concat({
      id: Date.now(),
      title,
      completed: false
    }));
  }

  function deleteItem(id) {
    setList(prevState => prevState.filter(item => item.id !== id));
  }

  return (
    <>
      <AddTodoItem addItem={addItem} />
      <TodoList
        list={list}
        completeItem={completeItem}
        changeItem={changeItem}
        deleteItem={deleteItem}
      />
    </>
  );
}

export default Todo;
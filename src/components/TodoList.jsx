import React from 'react';
import styled from 'styled-components';
import TodoItem from './TodoItem';

function TodoList({
  list,
  completeItem,
  changeItem,
  deleteItem
}) {
  let listItems;
  if (list.length) {
    listItems = (
      list.map(item => (
        <TodoItem
          key={item.id}
          id={item.id}
          title={item.title}
          completed={item.completed}
          completeItem={completeItem}
          changeItem={changeItem}
          deleteItem={deleteItem}
        />
      ))
    );
  } else {
    listItems = <p>Empty</p>;
  }

  return (
    <List children={listItems} />
  );
}

const List = styled.ul`
  padding: 0;
  list-style-type: none;
`;

export default TodoList;